﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PortalKolekcjonerow.Data;
using PortalKolekcjonerow.Models;

namespace PortalKolekcjonerow.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        ApplicationDbContext _dbContext;
        IWebHostEnvironment _environment;
        UserManager<IdentityUser> _userManager;

        public HomeController(ILogger<HomeController> logger, ApplicationDbContext dbContext, IWebHostEnvironment env, UserManager<IdentityUser> usermanager)
        {
            _logger = logger;
            _dbContext = dbContext;
            _environment = env;
            _userManager = usermanager;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async  Task<IActionResult> List()
        {
            IdentityUser user = new IdentityUser();
            var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);
            if(userId != null)
            {
                user = await _userManager.FindByIdAsync(userId);
                var roles = new List<string>();
                roles = await _userManager.GetRolesAsync(user) as List<string>;
                ViewBag.Role = roles[0];
            }
            
            return View(_dbContext.ArtykulyViewModels.ToList());
        }

        public IActionResult Details(int id)
        {
            ArtykulyViewModel artykuly = _dbContext.ArtykulyViewModels.Find(id);
            return View(artykuly);
        }

        [Authorize(Roles = "Redaktor, Administrator")]
        public IActionResult Create()
        {
            ArtykulyViewModel artykuly = new ArtykulyViewModel();
            return View(artykuly);
        }

        [Authorize(Roles = "Redaktor, Administrator")]
        [HttpPost]
        public async Task<IActionResult> Create(ArtykulyViewModel artykuly)
        {
            if (!ModelState.IsValid)
            {
                return View(artykuly);
            }
            else
            {
                string wwwRootPath = _environment.WebRootPath;
                string fileName = Path.GetFileNameWithoutExtension(artykuly.ImageFile.FileName);
                string extension = Path.GetExtension(artykuly.ImageFile.FileName);
                artykuly.ImageName = fileName = fileName + DateTime.Now.ToString("_ddMMyyyy_HHmm") + extension;
                string path = Path.Combine(wwwRootPath + "/Image/", fileName);
                using (var fileStream = new FileStream(path, FileMode.Create))
                {
                    await artykuly.ImageFile.CopyToAsync(fileStream);
                }

                artykuly.Date = DateTime.Now;
                var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);
                artykuly.IdAutora = userId;
                _dbContext.ArtykulyViewModels.Add(artykuly);
                _dbContext.SaveChanges();
                return RedirectToAction("Index");
            }
        }

        [Authorize(Roles = "Redaktor, Administrator")]
        public IActionResult Edit(int id)
        {
            ArtykulyViewModel artykuly = _dbContext.ArtykulyViewModels.Find(id);
            return View(artykuly);
        }

        [Authorize(Roles = "Redaktor, Administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(ArtykulyViewModel artykuly)
        {
            if (ModelState.IsValid)
            {
                var myEntinty = _dbContext.Entry(artykuly);
                myEntinty.State = EntityState.Modified;
                _dbContext.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(artykuly);
        }

        [Authorize(Roles = "Redaktor, Administrator")]
        public IActionResult Delete(int id)
        {
            ArtykulyViewModel artykuly = _dbContext.ArtykulyViewModels.Find(id);
            return View(artykuly);
        }

        [Authorize(Roles = "Redaktor, Administrator")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            ArtykulyViewModel artykuly = _dbContext.ArtykulyViewModels.Find(id);

            if(artykuly.ImageName != null)
            {
                var imagePath = Path.Combine(_environment.WebRootPath, "image", artykuly.ImageName);
                if (System.IO.File.Exists(imagePath))
                    System.IO.File.Delete(imagePath);
            }

            _dbContext.ArtykulyViewModels.Remove(artykuly);
            _dbContext.SaveChanges();
            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


    }
}

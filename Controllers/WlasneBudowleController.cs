﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PortalKolekcjonerow.Data;
using PortalKolekcjonerow.Models;

namespace PortalKolekcjonerow.Controllers
{
    public class WlasneBudowleController : Controller
    {
        ApplicationDbContext _dbContext;
        IWebHostEnvironment _environment;

        public WlasneBudowleController(ApplicationDbContext dbContext, IWebHostEnvironment environment)
        {
            _dbContext = dbContext;
            _environment = environment;
        }

        public IActionResult Index()
        {
            return View(_dbContext.WlasneBudowleViewModels.ToList());
        }

        [Authorize]
        public IActionResult Details(int id)
        {
            WlasneBudowleViewModel budowle = _dbContext.WlasneBudowleViewModels.Find(id);
            return View(budowle);
        }

        [Authorize]
        public IActionResult Create()
        {
            WlasneBudowleViewModel budowle = new WlasneBudowleViewModel();
            return View(budowle);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Create(WlasneBudowleViewModel budowle)
        {
            if (!ModelState.IsValid)
            {
                return View(budowle);
            }
            else
            {
                string wwwRootPath = _environment.WebRootPath;
                string fileName = Path.GetFileNameWithoutExtension(budowle.ImageFile.FileName);
                string extension = Path.GetExtension(budowle.ImageFile.FileName);
                budowle.ImageName = fileName = fileName + DateTime.Now.ToString("_ddMMyyyy_HHmm") + extension;
                string path = Path.Combine(wwwRootPath + "/Image/", fileName);
                using (var fileStream = new FileStream(path, FileMode.Create))
                {
                    await budowle.ImageFile.CopyToAsync(fileStream);
                }

                budowle.Date = DateTime.Now;
                var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);
                budowle.IdAutora = userId;
                _dbContext.WlasneBudowleViewModels.Add(budowle);
                _dbContext.SaveChanges();
                return RedirectToAction("Index");
            }
        }

        [Authorize]
        public IActionResult Edit(int id)
        {
            WlasneBudowleViewModel budowle = _dbContext.WlasneBudowleViewModels.Find(id);
            return View(budowle);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(WlasneBudowleViewModel budowle)
        {
            if (ModelState.IsValid)
            {
                var myEntinty = _dbContext.Entry(budowle);
                myEntinty.State = EntityState.Modified;
                _dbContext.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(budowle);
        }

        [Authorize]
        public IActionResult Delete(int id)
        {
            WlasneBudowleViewModel budowle = _dbContext.WlasneBudowleViewModels.Find(id);
            return View(budowle);
        }

        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            WlasneBudowleViewModel budowle = _dbContext.WlasneBudowleViewModels.Find(id);

            if (budowle.ImageName != null)
            {
                var imagePath = Path.Combine(_environment.WebRootPath, "image", budowle.ImageName);
                if (System.IO.File.Exists(imagePath))
                    System.IO.File.Delete(imagePath);
            }

            _dbContext.WlasneBudowleViewModels.Remove(budowle);
            _dbContext.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
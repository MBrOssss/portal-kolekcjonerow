﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PortalKolekcjonerow.Data;
using PortalKolekcjonerow.Models;

namespace PortalKolekcjonerow.Controllers
{
    public class OfertyController : Controller
    {
        ApplicationDbContext _dbContext;
        IWebHostEnvironment _environment;

        public OfertyController(ApplicationDbContext dbContext, IWebHostEnvironment environment)
        {
            _dbContext = dbContext;
            _environment = environment;
        }

        public IActionResult Index()
        {
            ViewBag.Message = TempData["Message"];
            return View(_dbContext.OfertyViewModels.ToList());
        }

        public IActionResult Details(int id)
        {
            OfertyViewModel oferty = _dbContext.OfertyViewModels.Find(id);
            return View(oferty);
        }

        [Authorize]
        public IActionResult Create()
        {
            OfertyViewModel oferty = new OfertyViewModel();
            return View(oferty);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Create(OfertyViewModel oferty)
        {
            if (!ModelState.IsValid)
            {
                return View(oferty);
            }
            else
            {
                string wwwRootPath = _environment.WebRootPath;
                string fileName = Path.GetFileNameWithoutExtension(oferty.ImageFile.FileName);
                string extension = Path.GetExtension(oferty.ImageFile.FileName);
                oferty.ImageName = fileName = fileName + DateTime.Now.ToString("_ddMMyyyy_HHmm") + extension;
                string path = Path.Combine(wwwRootPath + "/Image/", fileName);
                using (var fileStream = new FileStream(path, FileMode.Create))
                {
                    await oferty.ImageFile.CopyToAsync(fileStream);
                }

                oferty.Date = DateTime.Now;
                var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);
                oferty.IdSprzedawcy = userId;
                oferty.Status = "Dodana do bazy";
                _dbContext.OfertyViewModels.Add(oferty);
                _dbContext.SaveChanges();
                return RedirectToAction("Index");
            }
        }

        [Authorize]
        public IActionResult Edit(int id)
        {
            OfertyViewModel oferty = _dbContext.OfertyViewModels.Find(id);
            return View(oferty);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(OfertyViewModel oferty)
        {
            if (ModelState.IsValid)
            {
                var myEntinty = _dbContext.Entry(oferty);
                myEntinty.State = EntityState.Modified;
                _dbContext.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(oferty);
        }

        [Authorize]
        public IActionResult Delete(int id)
        {
            OfertyViewModel oferty = _dbContext.OfertyViewModels.Find(id);
            return View(oferty);
        }

        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            OfertyViewModel oferty = _dbContext.OfertyViewModels.Find(id);

            if (oferty.ImageName != null)
            {
                var imagePath = Path.Combine(_environment.WebRootPath, "image", oferty.ImageName);
                if (System.IO.File.Exists(imagePath))
                    System.IO.File.Delete(imagePath);
            }

            _dbContext.OfertyViewModels.Remove(oferty);
            _dbContext.SaveChanges();
            return RedirectToAction("Index");
        }

        [Authorize]
        public IActionResult Buy(int id)
        {
            OfertyViewModel oferta = _dbContext.OfertyViewModels.Find(id);
            var myEntinty = _dbContext.Entry(oferta);
            var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);

            if (oferta.IdSprzedawcy == userId || oferta.IdKupujacego != null)
            {
                TempData["Message"] = "Błąd!";
                return RedirectToAction("Index");
            }

            oferta.IdKupujacego = userId;
            oferta.Status = "Zarezerwowana";
            myEntinty.State = EntityState.Modified;
            _dbContext.SaveChanges();
            TempData["Message"] = "Zarezerwowano!";
            return RedirectToAction("Index");
        }
    }
}
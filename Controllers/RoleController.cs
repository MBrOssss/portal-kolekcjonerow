﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc.Rendering;
using PortalKolekcjonerow.Models;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.AspNetCore.Authorization;

namespace PortalKolekcjonerow.Controllers
{
    //[Authorize(Roles = "Administrator")]
    public class RoleController : Controller
    {
        RoleManager<IdentityRole> _roleManager;
        UserManager<IdentityUser> _userManager;

        public RoleController(RoleManager<IdentityRole> roleManager, UserManager<IdentityUser> userManager)
        {
            _roleManager = roleManager;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            var roles = _roleManager.Roles.ToList();
            return View(roles);
        }

        public IActionResult Create()
        {
            return View(new IdentityRole());
        }

        [HttpPost]
        public async Task<IActionResult> Create(IdentityRole role)
        {
            await _roleManager.CreateAsync(role);
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Assign(string roleName)
        {
            ViewBag.RoleName = roleName;
            var users = new List<UserRolesViewModel>();

            foreach(var u in _userManager.Users.ToList())
            {
                var user = new UserRolesViewModel();
                var roles = new List<string>();
                user.UserId = u.Id;
                user.UserEmail = u.Email;
                roles = await _userManager.GetRolesAsync(u) as List<string>;
                user.RoleName = roles[0];
                users.Add(user);
            }
            return View(users);
        }

        public async Task<IActionResult> AssignConfirmed(string id, string roleName, bool czyUsunac)
        {
            var user = await _userManager.FindByIdAsync(id);

            if (czyUsunac)
            {
                await _userManager.DeleteAsync(user);
            }
            else
            {
                foreach (var role in _roleManager.Roles.ToList())
                {
                    if (await _userManager.IsInRoleAsync(user, role.Name))
                    {
                        await _userManager.RemoveFromRoleAsync(user, role.Name);
                    }
                }

                await _userManager.AddToRoleAsync(user, roleName);
            }

            
            return RedirectToAction("Index");
        }
    }
}
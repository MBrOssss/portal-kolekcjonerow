﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;

namespace PortalKolekcjonerow.Models
{
    public class OfertyViewModel
    {
        [Key]
        public int IdOferty { get; set; }

        [Required]
        [Display(Name = "Tytuł")]
        public string Tytul { get; set; }

        [Required]
        [Display(Name = "Opis")]
        public string Opis { get; set; }

        public string IdSprzedawcy { get; set; }

        public string IdKupujacego { get; set; }

        [Display(Name = "Nazwa pliku")]
        public string ImageName { get; set; }

        [Display(Name = "Status")]
        public string Status { get; set; }

        [Display(Name = "Prześlij plik")]
        [NotMapped]
        public IFormFile ImageFile { get; set; }

        [Display(Name = "Data dodania")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? Date { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PortalKolekcjonerow.Models
{
    public class WlasneBudowleViewModel
    {
        [Key]
        public int IdBudowli { get; set; }

        [Required]
        [Display(Name = "Tytuł")]
        public string Tytul { get; set; }

        [Required]
        [Display(Name = "Opis")]
        public string Opis { get; set; }

        public string IdAutora { get; set; }

        [Display(Name = "Nazwa pliku")]
        public string ImageName { get; set; }

        [Display(Name = "Prześlij plik")]
        [NotMapped]
        public IFormFile ImageFile { get; set; }

        [Display(Name = "Data dodania")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? Date { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalKolekcjonerow.Models
{
    public class UserRolesViewModel
    {
        public string RoleName { get; set; }
        public string UserId { get; set; }
        public string UserEmail { get; set; }
    }
}

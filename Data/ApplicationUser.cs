﻿using Microsoft.AspNetCore.Identity;
using PortalKolekcjonerow.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalKolekcjonerow.Data
{
    public class ApplicationUser : IdentityUser
    {
        public List<ArtykulyViewModel> Artykuly { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PortalKolekcjonerow.Models;

namespace PortalKolekcjonerow.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<ArtykulyViewModel> ArtykulyViewModels { get; set; }
        public DbSet<WlasneBudowleViewModel> WlasneBudowleViewModels { get; set; }
        public DbSet<OfertyViewModel> OfertyViewModels { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<ApplicationUser>()
                .HasMany(pt => pt.Artykuly)
                .WithOne(p => p.User);

            builder.Entity<ArtykulyViewModel>()
                .HasOne(pt => pt.User)
                .WithMany(t => t.Artykuly);

            base.OnModelCreating(builder);
        }
    }
}

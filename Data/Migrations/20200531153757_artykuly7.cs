﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PortalKolekcjonerow.Data.Migrations
{
    public partial class artykuly7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Imagename",
                table: "ArtykulyViewModels",
                newName: "ImageName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ImageName",
                table: "ArtykulyViewModels",
                newName: "Imagename");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PortalKolekcjonerow.Data.Migrations
{
    public partial class artykuly3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "IdAutora",
                table: "ArtykulyViewModels",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "IdAutora",
                table: "ArtykulyViewModels",
                type: "int",
                nullable: false,
                oldClrType: typeof(string));
        }
    }
}

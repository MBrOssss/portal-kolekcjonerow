﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PortalKolekcjonerow.Data.Migrations
{
    public partial class artykuly4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "IdAutora",
                table: "ArtykulyViewModels",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "IdAutora",
                table: "ArtykulyViewModels",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}

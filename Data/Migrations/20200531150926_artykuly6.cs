﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PortalKolekcjonerow.Data.Migrations
{
    public partial class artykuly6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Image",
                table: "ArtykulyViewModels");

            migrationBuilder.AddColumn<string>(
                name: "Imagename",
                table: "ArtykulyViewModels",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Imagename",
                table: "ArtykulyViewModels");

            migrationBuilder.AddColumn<byte[]>(
                name: "Image",
                table: "ArtykulyViewModels",
                type: "varbinary(max)",
                nullable: true);
        }
    }
}

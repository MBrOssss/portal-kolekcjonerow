﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PortalKolekcjonerow.Data.Migrations
{
    public partial class wlasnebudowle : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WlasneBudowleViewModels",
                columns: table => new
                {
                    IdBudowli = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Tytul = table.Column<string>(nullable: false),
                    Opis = table.Column<string>(nullable: false),
                    IdAutora = table.Column<string>(nullable: true),
                    ImageName = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WlasneBudowleViewModels", x => x.IdBudowli);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WlasneBudowleViewModels");
        }
    }
}

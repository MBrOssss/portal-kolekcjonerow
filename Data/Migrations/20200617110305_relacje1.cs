﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PortalKolekcjonerow.Data.Migrations
{
    public partial class relacje1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "ArtykulyViewModels",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ArtykulyViewModels_UserId",
                table: "ArtykulyViewModels",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_ArtykulyViewModels_AspNetUsers_UserId",
                table: "ArtykulyViewModels",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ArtykulyViewModels_AspNetUsers_UserId",
                table: "ArtykulyViewModels");

            migrationBuilder.DropIndex(
                name: "IX_ArtykulyViewModels_UserId",
                table: "ArtykulyViewModels");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "ArtykulyViewModels");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PortalKolekcjonerow.Data.Migrations
{
    public partial class artykuly2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IdAutora",
                table: "ArtykulyViewModels",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IdAutora",
                table: "ArtykulyViewModels");
        }
    }
}
